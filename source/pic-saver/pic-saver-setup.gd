tool
extends Viewport

export (bool) var take_picture setget save_picture
export (bool) var update_viewports setget update
export (Vector2) var viewports_size setget set_size
export (String, FILE) var path



func save_picture(new_value):
	if Engine.editor_hint:
		take_picture = new_value
		var img = self.get_texture().get_data()
		img.flip_y()
		img.save_png(path)

func update(new_value):
	if Engine.editor_hint:
		update_viewports = new_value
		$Viewport.render_target_update_mode = Viewport.UPDATE_ONCE
		$Viewport.render_target_clear_mode = Viewport.CLEAR_MODE_ONLY_NEXT_FRAME
		self.render_target_update_mode = Viewport.UPDATE_ONCE
		self.render_target_clear_mode = Viewport.CLEAR_MODE_ONLY_NEXT_FRAME
	

func set_size(new_value):
	if Engine.editor_hint:
		viewports_size = new_value
		self.size = new_value
		$Viewport.size = new_value
		$Sprite.get_material().set_shader_param("size", new_value)

